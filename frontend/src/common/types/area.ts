export enum Area {
    Administration = 'Administración',
    Finance = 'Financiera',
    Purchase = 'Compras',
    Infrastructure = 'Infraestructura',
    Operation = 'Operación',
    VariousServices = 'Servicios Varios',
    HumanTalent = 'Talento Humano',
    Other = 'Otra',
}
