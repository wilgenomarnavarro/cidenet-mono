import { DocumentType } from '../types/documentType'
import { Area } from '../types/area'
import { Country } from '../types/country'

export const DocumentTypes = [
  DocumentType.CC,
  DocumentType.CE,
  DocumentType.Passport,
  DocumentType.SpecialPermission
] as DocumentType[]

export const Areas = [
  Area.Administration,
  Area.Finance,
  Area.HumanTalent,
  Area.Infrastructure,
  Area.Operation,
  Area.Purchase,
  Area.VariousServices,
  Area.Other
] as Area[]

export const Countries = [
  Country.Colombia,
  Country.American
] as Country[]

export const HeaderTable = [
  { text: 'Empleado', value: 'name', sortable: false },
  { text: 'Tipo de documento', value: 'document_type', sortable: false },
  { text: 'Identificacion', value: 'identification', sortable: false },
  { text: 'Area', value: 'area', sortable: false },
  { text: 'Pais', value: 'country', sortable: false },
  { text: 'Estado', value: 'status', sortable: false },
  { text: 'Correo', value: 'email', sortable: false },
  { text: 'Fecha de ingreso', value: 'income_date', sortable: false },
  { text: 'Fecha de creacion', value: 'created_at', sortable: false },
  { text: 'Fecha de actualizacion', value: 'updated_at', sortable: false },
  { text: 'Acciones', value: 'actions', sortable: false }
]
