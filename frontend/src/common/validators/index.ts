export const NameRule = [
  (v: string): boolean | string => !!v || 'Este campo es requerido',
  (v: string): boolean | string => /^[A-Z]*$/.test(v) || 'Solo permite caracteres de la A a la Z, mayúscula.',
  (v: string): boolean | string => v.length <= 20 || 'La longitud maxima son 20 caracteres.'
]

export const SurnameRule = [
  (v: string): boolean | string => !!v || 'Este campo es requerido',
  (v: string): boolean | string => /^[A-Z ]*$/.test(v) || 'Solo permite caracteres de la A a la Z, mayúscula.',
  (v: string): boolean | string => v.length <= 20 || 'La longitud maxima son 20 caracteres.'
]

export const IdentificationRule = [
  (v: string): boolean | string => !!v || 'Este campo es requerido',
  (v: string): boolean | string => /^[A-Za-z0-9-]*$/.test(v) || 'No se permiten caracteres especiales ni acentos.',
  (v: string): boolean | string => v.length <= 20 || 'La longitud maxima son 20 caracteres.'
]

export const OtherNamesRule = [
  (v: string): boolean | string => /^[A-Z ]*$/.test(v) || 'Solo permite caracteres de la A a la Z, mayúscula.',
  (v: string): boolean | string => v.length <= 50 || 'La longitud maxima son 20 caracteres.'
]
