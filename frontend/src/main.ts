import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import './filters/index'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import { AxiosStatic } from 'axios'
import { axiosInstance } from './plugins/axios'

Vue.config.productionTip = false
Vue.prototype.$axios = axiosInstance

declare module 'vue/types/vue' {
  interface Vue {
    $axios: AxiosStatic;
  }
}

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
