import Vue from 'vue'
import { User } from '../models/user'

Vue.filter('formatDate', function (value:string) :string {
  if (!value) return ''
  const date = new Date(value)
  return date.toLocaleDateString()
})

Vue.filter('formatDateTime', function (value:string) :string {
  if (!value) return ''
  const date = new Date(value)
  const numberOfMlSeconds = date.getTime()
  const addMlSeconds = 300 * 60000
  const newDate = new Date(numberOfMlSeconds - addMlSeconds)
  return newDate.toLocaleString()
})

Vue.filter('showFullName', function (user:User) :string {
  if (!user) return ''
  let fullName:string = user.first_name
  if (user.other_names && user.other_names.length) fullName += ` ${user.other_names}`
  fullName += ` ${user.first_surname} ${user.second_surname}`
  return fullName
})
