import request from "supertest";
import { app } from "../src/server";
import { UsersRepository } from "../src/repositories/users";
import { DocumentType } from "../src/common/types/documentType";
import { Country } from "../src/common/types/country";
import { Area } from "../src/common/types/area";
import { Status } from "../src/common/types/status";

describe("server", () => {
  test("/users returns all", async () => {
    const getAllSpy = jest
      .spyOn(UsersRepository.prototype, "all")
      .mockResolvedValue([
        {
          id: "1",
          first_name: "John",
          other_names: "",
          first_surname: "Perez",
          second_surname: "Mejia",
          country: Country.Colombia,
          document_type: DocumentType.CC,
          identification: "10545885",
          area: Area.Administration,
          email: "john.perez@cidenet.com.co",
          status: Status.Active,
          income_date: "2014/10/02",
          created_at: "2014/10/02",
          updated_at: "2014/10/02",
        }
      ]);

    const response = await request(app.callback()).get("/users");
    expect(getAllSpy).toHaveBeenCalled();
    expect(response.status).toBe(200);
    expect(response.text).toMatchSnapshot();
  });

  test("/users returns 500", async () => {
    const getAllSpy = jest
      .spyOn(UsersRepository.prototype, "all")
      .mockImplementation(() => {
        throw new Error("error");
      });

    const response = await request(app.callback()).get("/users");
    expect(getAllSpy).toHaveBeenCalled();
    expect(response.status).toBe(500);
    expect(response.text).toMatchSnapshot();
  });
});
