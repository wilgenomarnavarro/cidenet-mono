import { DocumentType } from '../common/types/documentType'
import { Area } from '../common/types/area'
import { Country } from '../common/types/country'
import { Status } from '../common/types/status'


export interface User {
    id: string;
    first_name: string;
    other_names: string;
    first_surname: string;
    second_surname: string;
    country: Country;
    document_type: DocumentType;
    identification: string;
    area: Area;
    email: string;
    status: Status.Active;
    income_date: string;
    created_at: string;
    updated_at: string;
}
