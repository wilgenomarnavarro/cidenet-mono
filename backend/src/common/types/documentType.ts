export enum DocumentType {
    CC = 'Cédula de Ciudadanía',
    CE = 'Cédula de Extranjería',
    Passport = 'Pasaporte',
    SpecialPermission = 'Permiso Especial'
}
