import { createPool } from "mysql2/promise"
console.log('process.env.DATABASE_HOST', process.env.DATABASE_HOST)
export const connection = createPool({
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    host: process.env.DATABASE_HOST,
    port: Number(process.env.DATABASE_PORT) || 3306
});

export const connectionMultipleStatement = createPool({
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    host: process.env.DATABASE_HOST,
    port: Number(process.env.DATABASE_PORT) || 3306,
    multipleStatements: true
});
