import {
  connection,
  connectionMultipleStatement,
} from "../common/mySqlConnection";
import { User } from "../domain/user";
import { DocumentType } from "../common/types/documentType";

export class UsersRepository {
  public async all(): Promise<User[]> {
    const [rows] = await connection.execute(
      "SELECT * FROM users ORDER BY name DESC"
    );

    return rows as User[];
  }

  public async getByFilterPagination(
    objectLimit: { limit:number, offset:number },
    first_name:string,
    other_names:string,
    first_surname:string,
    second_surname:string,
    country:string
  ): Promise<{ rows: User[]; count: number }> {
    const sentenceOrder = ` ORDER BY first_name ASC`
    let sentence:string = `SELECT * FROM users`
    let sentenceCount:string = `SELECT COUNT(*) as count FROM users`
    const object = this.buildQueryAndValues(first_name, other_names, first_surname, second_surname, country)
    let valuesSentence = object.values.slice()
    const valuesSentenceCount = object.values.slice()
    sentence += object.sentence
    sentenceCount += object.sentence
    sentence += sentenceOrder
    sentenceCount += sentenceOrder

    if (objectLimit.limit !== -1) {
      sentence += ` LIMIT ${objectLimit.offset}, ${objectLimit.limit}`
    }

    const [rows]: Array<any> = await connectionMultipleStatement.query(
      [sentenceCount, sentence].join("; "), [...valuesSentenceCount, ...valuesSentence]
    );

    const data: Array<User> = rows[1];
    const count: number = rows[0][0].count;
    return { rows: data, count: count };
  }

  public buildQueryAndValues(first_name:string, other_names:string, first_surname:string, second_surname:string, country:string) {
    let hasConditional = false
    let values = []
    let sentence = ' WHERE'
    if (first_name.length) {
      sentence += ` first_name LIKE ? `
      values.push(`%${first_name}%`)
      hasConditional = true
    }
    if (other_names.length) {
      sentence += hasConditional ? ` AND other_names LIKE ? ` : ' other_names LIKE ? '
      values.push(`%${other_names}%`)
      hasConditional = true
    }
    if (first_surname.length) {
      sentence += hasConditional ? ` AND first_surname LIKE ? ` : ` first_surname LIKE ? `
      values.push(`%${first_surname}%`)
      hasConditional = true
    }
    if (second_surname.length) {
      sentence += hasConditional ? ` AND second_surname LIKE ? ` : ` second_surname LIKE ? `
      values.push(`%${second_surname}%`)
      hasConditional = true
    }
    if (country.length) {
      sentence += hasConditional ? ` AND country = ? ` : ` country = ? `
      values.push(country)
      hasConditional = true
    }
    return hasConditional ? { sentence: sentence, values: values } : { sentence: '', values: values }
  }

  public async save(user: User): Promise<void> {
    const keysToQuery = Object.keys(user).join(", ");
    const valuesToQuery = Object.values(user);
    const scapes = Array(valuesToQuery.length).fill("?").join(", ");
    const sentence = `INSERT INTO users (${keysToQuery}) VALUES (${scapes})`;

    await connection.execute(sentence, valuesToQuery);
  }

  public async update(user: User): Promise<void> {
    let keysToQuery = Object.keys(user).join(" = ?, ");
    keysToQuery += ' = ?'
    const valuesToQuery = Object.values(user)
    valuesToQuery.push(user.id)
    const sentence = `UPDATE users SET ${keysToQuery} WHERE id = ?`;
    console.log("🚀 ~ file: users.ts ~ line 55 ~ UsersRepository ~ update ~ sentence", sentence)

    await connection.execute(sentence, valuesToQuery);
  }

  public async getByDocument(document_type:DocumentType, identification:string): Promise<User[]> {
    
    const sentence = `SELECT * from users WHERE document_type = ? AND identification = ? LIMIT 1`
    const [rows] = await connection.execute(sentence, [document_type, identification]);
    return rows as User[]
  }

  public async getById(id:string): Promise<User[]> {
    const sentence = `SELECT * from users WHERE id = ? LIMIT 1`
    const [rows] = await connection.execute(sentence, [id])
    return rows as User[]
  }

  public async getEmailsByUsernameEmail(email:string): Promise<User[]> {
    const likeEmail = `%${email}%`
    const sentence = `SELECT email from users WHERE email LIKE ? ORDER BY email ASC`
    console.log("🚀 ~ file: users.ts ~ line 108 ~ UsersRepository ~ getEmailsByUsernameEmail ~ sentence", sentence)
    const [rows] = await connection.execute(sentence, [likeEmail]);
    return rows as User[]
  }

  public async delteById(id:string): Promise<void> {    
    const sentence = `DELETE FROM users WHERE id = ?`
    await connection.execute(sentence, [id])
  }
}
