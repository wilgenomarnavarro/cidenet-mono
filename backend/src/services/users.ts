import { DocumentType } from "../common/types/documentType";
import { User } from "../domain/user";
import { v4 as uuid } from "uuid";
import { UsersRepository } from "../repositories/users";
const repository = new UsersRepository();

export class UsersService {
  public async create(user: User): Promise<User> {
    
    let users:User[] = await this.getByDocument(user.document_type, user.identification)
    if (users.length) {
        throw new Error('Ya existe un usuario con el mismo typo y numero de identificacion!')
    }
    user.email = await this.generateEmail(user)
    user.id = uuid();
    await repository.save(user)
    return user;       
  }

  public async delete(id:string): Promise<void> {
    await repository.delteById(id);
  }

  public async getAll(): Promise<User[]> {
    return await repository.all();
  }

  public async getByDocument(document_type:DocumentType, identification:string): Promise<User[]> {
    return await repository.getByDocument(document_type, identification);
  }

  public async getByFilterPagination(
    objectLimit: {limit:number, page:number},
    first_name:string,
    other_names:string,
    first_surname:string,
    second_surname:string,
    country:string
  ): Promise<{ rows: User[]; count: number }> {    
    return await repository.getByFilterPagination(
      { limit: objectLimit.limit, offset: (objectLimit.page - 1) * objectLimit.limit},
      first_name,
      other_names,
      first_surname,
      second_surname,
      country
    );
  }

  public async getById(id:string): Promise<User[]> {
    return await repository.getById(id);
  }

  public async getEmailsByUsernameEmail(username:string): Promise<User[]> {
    return await repository.getEmailsByUsernameEmail(username);
  }

  public async generateEmail(user:User):Promise<string> {
    const username = `${user.first_name.toLowerCase()}.${user.first_surname.replace(/\s/g, '').toLowerCase()}`
    const users = await this.getEmailsByUsernameEmail(username)

    if (users.length) {
      const arrayIndex:Array<number> = []
      users.forEach(user => {
        const idEmail = user.email.match(/\d+/)
        if(idEmail) arrayIndex.push(Number(idEmail[0]))
      })
      
      const id:number = arrayIndex.length ? Math.max(...arrayIndex) + 1 : 1
      const arrayEmail = user.email.split('@')
      
      return `${arrayEmail[0]}.${id}@${arrayEmail[1]}`
    } else {
      return user.email
    }
  }

  public async update(user: User): Promise<User> {    
    let users:User[] = await this.getByDocument(user.document_type, user.identification)
    if (users.length && users[0].id !== user.id) {
        throw new Error('Ya existe un usuario con el mismo typo y numero de identificacion!')
    }
    if (!user.email.match(/\d+/)){
      user.email = await this.generateEmail(user)
    }

    await repository.update(user)
    const userUpdated = await repository.getById(user.id)
    return userUpdated[0]
  }
}
