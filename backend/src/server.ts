import Koa from 'koa'
import KoaRouter from 'koa-router'
import koaBody from 'koa-body'
import json from 'koa-json'
import KoaLoger from 'koa-logger'
import dotenv from 'dotenv'
import cors from '@koa/cors'
dotenv.config()
import { userRouter } from './routes/users'

const logger = KoaLoger()
const router = new KoaRouter({ prefix: '/api/v1' })
const app = new Koa()
app.use(cors());
app.use(json())
app.use(koaBody({
    multipart: true,
    urlencoded: true
  }))

app.use(logger)
userRouter(router)
app.use(router.routes())
app.use(router.allowedMethods())


/* APP USE */
app.listen(3000)

export {app}
