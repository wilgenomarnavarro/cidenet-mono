import Router from "koa-router";
import { UsersService } from "../services/users";
import { User } from "../domain/user";
const usersServices = new UsersService();

export const userRouter = (router: Router): void => {
  router.get("/users", async (ctx) => {
    const {
      limit,
      page,
      first_name,
      other_names,
      first_surname,
      second_surname,
      country
    } = ctx.request.query!;

    try {
      ctx.body = await usersServices.getByFilterPagination(
        { limit: Number(limit), page: Number(page) },
        String(first_name),
        String(other_names),
        String(first_surname),
        String(second_surname),
        String(country)
      );
    } catch (error) {
      console.error(error);
      ctx.status = 500;
    }
  });

  router.post("/users", async (ctx) => {
    const user: User = ctx.request.body.user;

    try {
      ctx.body = await usersServices.create(user);
    } catch (error) {
      ctx.status = 500;
      ctx.body = { message: (error as Error).message };
    }
  });

  router.delete("/users/:id", async (ctx) => {
    try {
      await usersServices.delete(ctx.params.id);
      ctx.status = 200;
    } catch (error) {
      ctx.status = 500;
      ctx.body = { message: (error as Error).message };
    }
  });

  router.put("/users", async (ctx) => {
    const user: User = ctx.request.body.user;
    try {
      ctx.body = await usersServices.update(user);
    } catch (error) {
      ctx.status = 500;
      ctx.body = { message: (error as Error).message };
    }
  });
};
